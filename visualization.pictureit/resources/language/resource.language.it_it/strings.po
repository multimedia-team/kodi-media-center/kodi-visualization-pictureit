# Kodi Media Center language file
# Addon Name: PictureIt
# Addon id: visualization.pictureit
# Addon Provider: linuxwhatelse
msgid ""
msgstr ""
"Project-Id-Version: visualization.pictureit\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2023-11-08 21:40+0000\n"
"Last-Translator: Massimo Pissarello <mapi68@gmail.com>\n"
"Language-Team: Italian <https://kodi.weblate.cloud/projects/kodi-add-ons-look-and-feel/visualization-pictureit/it_it/>\n"
"Language: it_it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.1\n"

msgctxt "Addon Summary"
msgid "PictureIt - Kodi Visualization"
msgstr "PictureIt - Visualizzazione Kodi"

msgctxt "Addon Description"
msgid "PictureIt is supposed to make listening to music more elegant and less flashy.[CR]The idea is to take random images YOU selected and display them alongside a simple spectrum at the bottom.[CR]Simple right?[CR][CR]Features:[CR]- Select a local directory containing your images (If sub-folders are present, those will be used as \"Preset\")[CR]- Update images by interval[CR]- Update images on new track[CR]- Change the transition time[CR]- Enable/Disable the spectrum[CR]- Enable/Disable a settle spectrum-background[CR]- Change the spectrums: Width, Bottom-Padding, Animation-Speed"
msgstr "PictureIt dovrebbe rendere l'ascolto della musica più elegante e meno appariscente.[CR]L'idea è quella di prendere immagini casuali selezionate da TE e mostrarle insieme a un semplice spettro nella parte inferiore.[CR]Semplice vero?[CR][CR]Caratteristiche:[CR]- Seleziona una cartella locale contenente le tue immagini (se sono presenti delle sottocartelle, quelle verranno usate come \"Preset\")[CR]- Aggiorna le immagini per intervallo[CR]- Aggiorna le immagini su una nuova traccia[CR]- Modifica il tempo di transizione [CR]- Abilita o disabilita lo spettro[CR]- Abilita o disabilita uno sfondo dello spettro di stabilizzazione[CR]- Modifica gli spettri: Larghezza, Padding-Basso, Animazione-Velocità"

msgctxt "#30000"
msgid "General"
msgstr "Generale"

msgctxt "#30001"
msgid "Presets root directory"
msgstr "Preimpostazioni cartella principale"

msgctxt "#30002"
msgid "Transition time (ms)"
msgstr "Tempo transizione (ms)"

msgctxt "#30003"
msgid "Update on new track"
msgstr "Aggiorna su nuova traccia"

msgctxt "#30004"
msgid "New image by interval"
msgstr "Nuova immagine per intervallo"

msgctxt "#30005"
msgid "Image update interval (sec)"
msgstr "Intervallo aggiornamento immagine (sec)"

msgctxt "#30006"
msgid "Spectrum"
msgstr "Spettro"

msgctxt "#30007"
msgid "Enable spectrum"
msgstr "Abilita spettro"

msgctxt "#30008"
msgid "Show spectrum background"
msgstr "Mostra lo sfondo dello spettro"

msgctxt "#30009"
msgid "Width"
msgstr "Larghezza"

msgctxt "#30010"
msgid "Padding bottom"
msgstr "Padding basso"

msgctxt "#30011"
msgid "Animation speed"
msgstr "Velocità animazione"
